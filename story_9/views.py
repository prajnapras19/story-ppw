from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

def signup(request):
    response = {}
    response['form'] = UserCreationForm
    if (request.user.is_authenticated):
        return redirect('home')
    if (request.method == 'POST'):
        form = UserCreationForm(request.POST)
        if (form.is_valid()):
            user = form.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            request.session['username'] = user.username
            return redirect('home')
        else:
            response['form'] = form
    return render(request, 'story_9/signup.html', response)
    
def signin(request):
    response = {}
    if (request.user.is_authenticated):
        return redirect('home')
    if (request.method == 'GET'):
        return render(request, 'story_9/signin.html')
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        request.session['username'] = username
        return redirect('home')
    else:
        response['message'] = 'Username / Password salah'
    return render(request, 'story_9/signin.html', response)
    
def signout(request):
    logout(request)
    request.session.flush()
    return redirect('home')
