from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.http import JsonResponse
import requests
import json

class TestStory9(TestCase):
    def test_url_signup_api_exists(self):
        response = Client().get('/accounts/signup')
        self.assertEqual(response.status_code, 200)
    
    def test_url_signin_exists(self):
        response = Client().get('/accounts/signin')
        self.assertEqual(response.status_code, 200)
        
    def test_url_signout_exists(self):
        response = Client().get('/accounts/signout')
        self.assertEqual(response.status_code, 302)
    
    def test_signup_using_views(self):
        resolver = resolve('/accounts/signup')
        self.assertEqual(resolver.func.__name__, signup.__name__)
        
    def test_signin_using_views(self):
        resolver = resolve('/accounts/signin')
        self.assertEqual(resolver.func.__name__, signin.__name__)
        
    def test_signout_using_views(self):
        resolver = resolve('/accounts/signout')
        self.assertEqual(resolver.func.__name__, signout.__name__)
        
    def test_signup_using_template_story_9(self):
        response = Client().get('/accounts/signup')
        self.assertTemplateUsed(response, 'story_9/signup.html')
        
    def test_signin_using_template_story_9(self):
        response = Client().get('/accounts/signin')
        self.assertTemplateUsed(response, 'story_9/signin.html')

    def test_signup_success(self):
        data = {'username':'coba', 'password1': 'inipassword', 'password2': 'inipassword'}
        response = Client().post('/accounts/signup', data)
        self.assertEqual(response.status_code, 302)
    
    def test_signin_success(self):
        data = {'username':'coba', 'password1': 'inipassword', 'password2': 'inipassword'}
        Client().post('/accounts/signup', data)
        Client().get('/accounts/signout', data)
        data = {'username':'coba', 'password': 'inipassword'}
        response = Client().post('/accounts/signin', data)
        self.assertEqual(response.status_code, 302)
        
    def test_signout_success(self):
        data = {'username':'coba', 'password1': 'inipassword', 'password2': 'inipassword'}
        Client().post('/accounts/signup', data)
        response = Client().get('/accounts/signout', data)
        self.assertEqual(response.status_code, 302)
        
