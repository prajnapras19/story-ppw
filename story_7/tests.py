from django.test import TestCase, Client

class TestStory7(TestCase):
    def test_url_stories_exists(self):
        response = Client().get('/stories')
        self.assertEqual(response.status_code, 200)
    
    def test_recent_activities_in_stories(self):
        response = Client().get('/stories')
        content = response.content.decode('utf-8')
        self.assertIn("Recent Activities", content)
        
    def test_organizations_joined_in_stories(self):
        response = Client().get('/stories')
        content = response.content.decode('utf-8')
        self.assertIn("Organizations Joined", content)
        
    def test_volunteering_experiences_in_stories(self):
        response = Client().get('/stories')
        content = response.content.decode('utf-8')
        self.assertIn("Volunteering Experiences", content)
        
    def test_achievements_in_stories(self):
        response = Client().get('/stories')
        content = response.content.decode('utf-8')
        self.assertIn("Achievements", content)
    
    def test_using_jquery_in_page(self):
        response = Client().get('/stories')
        content = response.content.decode('utf-8')
        self.assertIn('<script src="/static/jquery-3.5.1.js"></script>', content)
        self.assertIn('<script src="/static/jquery-ui.js"></script>', content)
        
    def test_accordion_in_stories(self):
        response = Client().get('/stories')
        content = response.content.decode('utf-8')
        self.assertIn('<div id="accordion">', content)
