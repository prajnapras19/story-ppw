from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('projects', views.projects, name='projects'),
    path('stories', views.stories, name='stories'),
    path('gallery', views.gallery, name='gallery'),
]
