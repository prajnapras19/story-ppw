from django.shortcuts import render
from story_5.models import Course

def index(request):
    return render(request, 'story_4/index.html')
    
def projects(request):
    return render(request, 'story_4/projects.html')
    
def stories(request):
    courses = Course.objects.all()
    response = {'courses':courses}
    return render(request, 'story_4/stories.html', response)
    
def gallery(request):
    return render(request, 'story_4/misc.html')
