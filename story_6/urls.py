from django.urls import path
from . import views

urlpatterns = [
    path('', views.event, name='events'),
    path('add', views.addEvent, name='add_event'),
    path('delete', views.deleteEvent, name='delete_event'),
    path('<int:event_id>/register', views.addParticipant, name='add_participant'),
    path('delete-participants', views.deleteParticipant, name='delete_participant'),
]
