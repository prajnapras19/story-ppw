from django.shortcuts import render
from .forms import *
from .models import *

def event(request):
    events = Event.objects.all()
    events_with_participants_list = []
    for event in events:
        participants = Participant.objects.filter(event=event)
        events_with_participants_list.append([event, participants])
    if (len(events_with_participants_list) > 0):
        response = {'events' : events_with_participants_list}
    else:
        response = {}
    return render(request, 'story_6/events.html', response)

def addEvent(request):
    response = {'form' : AddEventForm}
    if (request.method == 'POST'):
        form = AddEventForm(request.POST)
        if (form.is_valid()):
            form.save()
            response['status'] = 'Event added successfully'
    return render(request, 'story_6/add_event.html', response)
    
def addParticipant(request, event_id):
    response = {'form' : AddParticipantForm, 'event_id' : event_id}
    try:
        event_name = Event.objects.filter(id=event_id)[0]
    except:
        return render(request, 'story_6/add_participant.html')
    response['event_name'] = event_name
    if (request.method == 'POST'):
        form = AddParticipantForm(request.POST)
        if (form.is_valid()):
            participant = form.save(commit=False)
            participant.event = Event.objects.filter(id=event_id)[0]
            participant.save()
            response['status'] = 'Successfully registered to the event'
    return render(request, 'story_6/add_participant.html', response)

def deleteEvent(request):
    response = {}
    if (request.method == 'POST'):
        deleted = request.POST.getlist('deleted')
        response['status'] = 'Select event(s) to delete!'
        for event_id in deleted:
            Event.objects.filter(id=event_id).delete()
            response['status'] = 'Event(s) deleted successfully'
    events = Event.objects.all()
    response['events'] = events
    return render(request, 'story_6/delete_event.html', response)

def deleteParticipant(request):
    response = {}
    if (request.method == 'POST'):
        deleted = request.POST.getlist('deleted')
        response['status'] = 'Select participant(s) to delete!'
        for participant_id in deleted:
            Participant.objects.filter(id=participant_id).delete()
            response['status'] = 'Participant(s) deleted successfully'
    participants = Participant.objects.all()
    response['participants'] = participants
    return render(request, 'story_6/delete_participant.html', response)
