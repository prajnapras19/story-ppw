from django.test import TestCase, Client
from django.urls import resolve
from .models import *
from . import views, forms
import dateutil.parser


class TestStory6(TestCase):
    # models test
    
    def test_add_event(self):
        new_event = Event.objects.create(
            name='new event',
            date = dateutil.parser.parse('01/01/2020').date(),
            place = 'online',
            description = 'a new event',
        )
        event = Event.objects.get(name='new event')
        self.assertEqual(event.name, 'new event')
        self.assertEqual(event.date, dateutil.parser.parse('01/01/2020').date())
        self.assertEqual(event.place, 'online')
        self.assertEqual(event.description, 'a new event')
    
    def test_add_participant(self):
        new_event = Event.objects.create(
            name='new event',
            date = dateutil.parser.parse('01/01/2020').date(),
            place = 'online',
            description = 'a new event',
        )
        event = Event.objects.get(name='new event')
        new_participant = Participant.objects.create(
            name='Kak Pewe',
            event = event,
        )
        participant = Participant.objects.get(event=event)
        self.assertEqual(participant.name, 'Kak Pewe')
        self.assertEqual(participant.event, event)
    
    
    # url test
    
    def test_add_event_url_exists(self):
        response = Client().get('/events/add')
        self.assertEquals(response.status_code, 200)
    
    def test_add_participant_url_exists(self):
        response = Client().get('/events/1/register')
        self.assertEquals(response.status_code, 200)
    
    def test_delete_event_url_exists(self):
        response = Client().get('/events/delete')
        self.assertEquals(response.status_code, 200)
    
    def test_delete_participant_url_exists(self):
        response = Client().get('/events/delete-participants')
        self.assertEquals(response.status_code, 200)
    
    # views test
    
    def test_event_view(self):
        func = resolve('/events/')
        self.assertEqual(func.func, views.event)
    
    def test_matching_add_event_view(self):
        func = resolve('/events/add')
        self.assertEqual(func.func, views.addEvent)

    def test_matching_add_participant_view(self):
        func = resolve('/events/1/register')
        self.assertEqual(func.func, views.addParticipant)
        
    def test_matching_delete_event_view(self):
        func = resolve('/events/delete')
        self.assertEqual(func.func, views.deleteEvent)
    
    def test_matching_delete_participant_view(self):
        func = resolve('/events/delete-participants')
        self.assertEqual(func.func, views.deleteParticipant)
    
    def test_add_event_post_request(self):
        response = Client().post('/events/add', data={
            'name' : 'new event',
            'date' : '2020-01-01',
            'place' : 'online',
            'description' : 'a new event',
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Event added successfully', response.content.decode('utf-8'))
    
    def test_add_participant_post_request_event_exists(self):
        new_event = Event.objects.create(
            name='new event',
            date = dateutil.parser.parse('01/01/2020').date(),
            place = 'online',
            description = 'a new event',
        )
        response = Client().post('/events/1/register', data={
            'name' : 'new participant',
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Successfully registered to the event', response.content.decode('utf-8'))
    
    def test_add_participant_post_request_event_not_exists(self):
        response = Client().post('/events/1/register', data={
            'name' : 'new participant',
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Event not found!', response.content.decode('utf-8'))
    
    def test_delete_event_post_request_event_exists(self):
        new_event = Event.objects.create(
            name='new event',
            date = dateutil.parser.parse('01/01/2020').date(),
            place = 'online',
            description = 'a new event',
        )
        response = Client().post('/events/delete', data={
            'deleted' : [1],
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Event(s) deleted successfully', response.content.decode('utf-8'))
    
    def test_delete_event_post_request_event_not_exists(self):
        response = Client().post('/events/delete', data={
            'deleted' : [],
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Select event(s) to delete!', response.content.decode('utf-8'))
        
    def test_delete_participant_post_request_participant_exists(self):
        new_event = Event.objects.create(
            name='new event',
            date = dateutil.parser.parse('01/01/2020').date(),
            place = 'online',
            description = 'a new event',
        )
        event = Event.objects.get(name='new event')
        new_participant = Participant.objects.create(
            name='Kak Pewe',
            event = event,
        )
        response = Client().post('/events/delete-participants', data={
            'deleted' : [1],
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Participant(s) deleted successfully', response.content.decode('utf-8'))
        
    def test_delete_participant_post_request_participant_not_exists(self):
        response = Client().post('/events/delete-participants', data={
            'deleted' : [],
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Select participant(s) to delete!', response.content.decode('utf-8'))
        
        
    # template test
    def test_template_event_with_event_exists(self):
        new_event = Event.objects.create(
            name='new event',
            date = dateutil.parser.parse('01/01/2020').date(),
            place = 'online',
            description = 'a new event',
        )
        response = Client().get('/events/')
        self.assertIn('new event', response.content.decode('utf-8'))
        self.assertTemplateUsed(response, 'story_6/events.html')
        
    def test_template_event_with_event_not_exists(self):
        response = Client().get('/events/')
        self.assertIn('There are no event(s) right now.', response.content.decode('utf-8'))
        self.assertTemplateUsed(response, 'story_6/events.html')
    
    def test_template_add_event(self):
        response = Client().get('/events/add')
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Add Event</h1>', content)
        self.assertIn('Name:', content)
        self.assertIn('Date:', content)
        self.assertIn('Place:', content)
        self.assertIn('Description:', content)
        self.assertTemplateUsed(response, 'story_6/add_event.html')
    
    def test_template_add_participant_event_not_exists(self):
        response = Client().get('/events/1/register')
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Register to The Event</h1>', content)
        self.assertIn('Event not found!', content)
        self.assertTemplateUsed(response, 'story_6/add_participant.html')
    
    def test_template_add_participant_event_exists(self):
        new_event = Event.objects.create(
            name='new event',
            date = dateutil.parser.parse('01/01/2020').date(),
            place = 'online',
            description = 'a new event',
        )
        response = Client().get('/events/1/register')
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Register to The Event</h1>', content)
        self.assertIn('Name:', content)
        self.assertTemplateUsed(response, 'story_6/add_participant.html')
