from django.db import models
from django.utils.timezone import now

class Event(models.Model):
    name = models.CharField(max_length=200)
    date = models.DateField(default=now)
    place = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    def __str__(self):
        return self.name
    
class Participant(models.Model):
    name = models.CharField(max_length=200)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    def __str__(self):
        return self.name + ' - ' + str(self.event)
