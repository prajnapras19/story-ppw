from django import forms
from .models import *

class AddEventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'date', 'place', 'description']
    
    name = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs={'autocomplete':'off'}))
    date = forms.DateField(required=True)
    place = forms.CharField(max_length=200, required = True, widget=forms.TextInput(attrs={'autocomplete':'off'}))
    description = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs={'autocomplete':'off'}))
    
class AddParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = ['name']
        exclude = ['event']
        
    name = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs={'autocomplete':'off'}))
