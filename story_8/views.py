from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json

def search(request):
    if (not request.user.is_authenticated):
        return redirect('signin')
    return render(request, 'story_8/search.html')
    
def searchAPI(request):
    api_url = 'https://www.googleapis.com/books/v1/volumes'
    try:
        keyword = request.GET['q']
    except:
        keyword = ''
    if (keyword == ''):
        return JsonResponse({'message': 'No query entered'})
    response = requests.get(url=api_url, params={'q': keyword}).content
    response = json.loads(response)
    return JsonResponse(response, safe=False)
