from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.http import JsonResponse
import requests
import json

class TestStory8(TestCase):
    def test_url_search_exists(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 302)
    
    def test_url_search_api_exists(self):
        response = Client().get('/search/api')
        self.assertEqual(response.status_code, 200)
    
    def test_search_using_views(self):
        resolver = resolve('/search/')
        self.assertEqual(resolver.func.__name__, search.__name__)
    
    def test_search_api_using_views(self):
        resolver = resolve('/search/api')
        self.assertEqual(resolver.func.__name__, searchAPI.__name__)
        
    '''
    def test_search_api_works(self):
        api_url = 'https://www.googleapis.com/books/v1/volumes'
        keyword = 'abc'
        response1 = requests.get(url=api_url, params={'q': keyword}).content
        response1 = JsonResponse(json.loads(response1), safe=False).content
        response2 = Client().get('/search/api?q=%s' % keyword).content
        self.assertEqual(response1, response2)
    '''
    '''
    def test_using_template_story_8(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'story_8/search.html')
    '''
