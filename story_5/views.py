from django.shortcuts import render
from .models import Course
from .forms import AddForm

def add(request):
    response = {'form' : AddForm}
    if (request.method == 'POST'):
        form = AddForm(request.POST or None)
        if (form.is_valid()):
            form.save()
            response['status'] = 'Course added successfully'
    return render(request, 'story_5/add.html', response)

def delete(request):
    response = {}
    if (request.method == 'POST'):
        deleted = request.POST.getlist('deleted')
        response['status'] = 'Select course(s) to delete!'
        for course_id in deleted:
            try:
                Course.objects.filter(id=course_id).delete()
                response['status'] = 'Course(s) deleted successfully'
            except:
                pass
    courses = Course.objects.all()
    response['courses'] = courses
    return render(request, 'story_5/delete.html', response)
