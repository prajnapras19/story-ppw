from django import forms
from .models import Course

class AddForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['course_name', 'lecturer', 'credits', 'description']
    
    attrs = {'autocomplete':'off'}
    course_name = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs=attrs))
    lecturer = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs=attrs))
    credits = forms.IntegerField(required = True, widget=forms.NumberInput(attrs=attrs))
    description = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs=attrs))
