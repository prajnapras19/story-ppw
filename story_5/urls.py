from django.urls import path
from . import views

urlpatterns = [
    path('add', views.add, name='add_course'),
    path('delete', views.delete, name='delete_course'),
]
