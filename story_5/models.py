from django.db import models

class Course(models.Model):
    course_name = models.CharField(max_length=200)
    lecturer = models.CharField(max_length=200)
    credits = models.IntegerField()
    description = models.CharField(max_length=200)
